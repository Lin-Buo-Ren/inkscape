# SPDX-License-Identifier: GPL-2.0-or-later
set(gimpwidgets_SRC
	gimpcolorwheel.c
	ruler.cpp

	# -------
	# Headers
	gimpcolorwheel.h
	ruler.h
)

add_inkscape_source("${gimpwidgets_SRC}")
